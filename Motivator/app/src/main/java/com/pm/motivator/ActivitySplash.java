package com.pm.motivator;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ActivitySplash extends AppCompatActivity {
    private FragmentActivity mActivity = null;

    private int splashTime = 3000;

    private Handler handler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mActivity = this;

        handler = new Handler();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void onResume() {
        handler.postDelayed(runnable, splashTime);
        super.onResume();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            startActivity(new Intent(mActivity, ActivitySignIn.class));

            finish();
        }
    };
}
