package com.pm.motivator.networks.dialogs.npd;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.motivator.R;
/**
 * Created by faisalwahab on 10/12/15.
 */
public class HUDProgress implements IProgressDialog {
    private Dialog pd = null;

    private static HUDProgress INSTANCE = null;

    public static HUDProgress getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new HUDProgress();
        }
        return INSTANCE;
    }

    public static HUDProgress newInstance() {
        return new HUDProgress();
    }

    private HUDProgress() {
    }

    @Override
    public void init(Activity activity) {
        pd = new Dialog(activity, R.style.ProgressHUD);
        pd.setContentView(R.layout.progress_hud);

        pd.setCancelable(false);
        pd.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = pd.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        pd.getWindow().setAttributes(lp);
    }

    @Override
    public void title(String title) {
        if (pd != null) {
            if (title != null && title.length() > 0) {
                pd.setTitle(title);
            } else {
                pd.setTitle("");
            }
        }
    }

    @Override
    public void message(String message) {
        if (pd != null) {
            TextView txt = (TextView) pd.findViewById(R.id.message);
            if (message != null && message.length() > 0) {
                txt.setText(message);
            } else {
                txt.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void show() {
        if (pd != null) {
            pd.show();
            onWindowFocusChanged(true);
        }
    }

    @Override
    public void hide() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (pd != null) {
            ImageView imageView = (ImageView) pd.findViewById(R.id.spinnerImageView);
            AnimationDrawable spinner = (AnimationDrawable) imageView.getBackground();
            spinner.start();
        }
    }

}
