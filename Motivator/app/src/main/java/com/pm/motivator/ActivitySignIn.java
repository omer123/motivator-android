package com.pm.motivator;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivitySignIn extends AppCompatActivity implements View.OnClickListener {
    private FragmentActivity mActivity = null;

    private EditText et_email = null;
    private EditText et_password = null;
    private Button btn_signin = null;
    private TextView tv_singup = null;

    private String strEmail = null;
    private String strPassword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mActivity = this;


        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_signin = (Button) findViewById(R.id.btn_signin);
        tv_singup = (TextView) findViewById(R.id.tv_singup);

        btn_signin.setOnClickListener(this);
        tv_singup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                strEmail = et_email.getText().toString();
                strPassword = et_password.getText().toString();

                startActivity(new Intent(mActivity, ActivityMain.class));
                finish();
                break;
            case R.id.tv_singup:
                startActivity(new Intent(mActivity, ActivitySignUp.class));
                finish();
                break;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
