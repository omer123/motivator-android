package com.pm.motivator.models;

import org.parceler.Parcel;

/**
 * Created by faisalwahab on 10/20/15.
 */
@Parcel
public class ModelHabit {
    private String urlHabitBg = null;
    private boolean isHabitIconShow = false;
    private String textHabit = null;

    public ModelHabit() {
    }

    public ModelHabit(String urlHabitBg, boolean isHabitIconShow, String textHabit) {
        this.urlHabitBg = urlHabitBg;
        this.isHabitIconShow = isHabitIconShow;
        this.textHabit = textHabit;
    }

    public String getUrlHabitBg() {
        return urlHabitBg;
    }

    public void setUrlHabitBg(String urlHabitBg) {
        this.urlHabitBg = urlHabitBg;
    }

    public boolean isHabitIconShow() {
        return isHabitIconShow;
    }

    public void setIsHabitIconShow(boolean isHabitIconShow) {
        this.isHabitIconShow = isHabitIconShow;
    }

    public String getTextHabit() {
        return textHabit;
    }

    public void setTextHabit(String textHabit) {
        this.textHabit = textHabit;
    }


    @Override
    public String toString() {
        return "ModelHabit{" +
                "urlHabitBg='" + urlHabitBg + '\'' +
                ", isHabitIconShow=" + isHabitIconShow +
                ", textHabit='" + textHabit + '\'' +
                '}';
    }
}
