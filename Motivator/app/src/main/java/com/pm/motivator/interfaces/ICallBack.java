package com.pm.motivator.interfaces;

public interface ICallBack {
	void notify(Object object, String action);
}
