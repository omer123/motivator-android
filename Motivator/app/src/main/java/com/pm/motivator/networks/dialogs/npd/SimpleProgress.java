package com.pm.motivator.networks.dialogs.npd;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;

/**
 * Created by faisalwahab on 10/12/15.
 */
public class SimpleProgress implements IProgressDialog {
    private ProgressDialog pd = null;


    private static SimpleProgress INSTANCE = null;

    public static SimpleProgress getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SimpleProgress();
        }
        return INSTANCE;
    }

    public static SimpleProgress newInstance() {
        return new SimpleProgress();
    }
    private SimpleProgress() {
    }

    @Override
    public void init(Activity activity) {
        pd = new ProgressDialog(activity, android.R.style.Theme_Holo_Light_Dialog);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pd.getWindow().setGravity(Gravity.CENTER);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        message("Loading...");
    }

    @Override
    public void title(String title) {
        if (pd != null) {
            if (title != null && title.length() > 0) {
                pd.setTitle(title);
            } else {
                pd.setTitle("");
            }
        }
    }

    @Override
    public void message(String message) {
        if (pd != null) {
            if (message != null && message.length() > 0) {
                pd.setMessage(message);
            } else {
                pd.setMessage("");
            }
        }
    }

    @Override
    public void show() {
        if (pd != null) {
            pd.show();
        }
    }

    @Override
    public void hide() {
        if (pd != null) {
            pd.dismiss();
        }
    }

}
