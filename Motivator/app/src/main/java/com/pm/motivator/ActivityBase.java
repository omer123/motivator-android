package com.pm.motivator;

;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import android.widget.LinearLayout;


public class ActivityBase extends AppCompatActivity implements OnClickListener {
	public FragmentActivity mActivity = null;

	public LinearLayout ll_btn_back = null;
	public LinearLayout ll_btn_edit = null;

	private boolean isBackButtonEnable = false;
	private boolean isEditButtonEnable = false;

	public ActivityBase(boolean btnBackEnable,boolean btnEditEnable) {
		isBackButtonEnable = btnBackEnable;
		isEditButtonEnable = btnEditEnable;
	}

	public ActivityBase() {
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = this;

		// set the custom action bar
		if(android.os.Build.VERSION.SDK_INT < 11) {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		}
		View addView = getLayoutInflater().inflate(R.layout.action_bar, null);
		getSupportActionBar().setCustomView(addView);
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//		getActionBar().setHomeButtonEnabled(false);

		ll_btn_back = (LinearLayout) addView.findViewById(R.id.ll_btn_back);
		ll_btn_edit = (LinearLayout) addView.findViewById(R.id.ll_btn_edit);

		ll_btn_back.setOnClickListener(this);
		ll_btn_edit.setOnClickListener(this);

		if (isBackButtonEnable){
			ll_btn_back.setVisibility(View.VISIBLE);
		}else{
			ll_btn_back.setVisibility(View.GONE);
		}
		if (isEditButtonEnable){
			ll_btn_edit.setVisibility(View.VISIBLE);
		}else{
			ll_btn_edit.setVisibility(View.GONE);
		}

//		if (mTitleInt != 0) {
//			mTitleStr = mActivity.getResources().getString(mTitleInt);
//		}
//		mTitleStr = mTitleStr.toUpperCase();
//
//		if (mTitleStr != null && !mTitleStr.isEmpty()) {
//			iv_logo.setVisibility(View.GONE);
//			tv_header.setVisibility(View.VISIBLE);
//			tv_header.setText(mTitleStr);
//
//			if (mTitleStr.equalsIgnoreCase("New Case") || mTitleStr.equalsIgnoreCase("NY Case")) {
//				ll_btn_back.setVisibility(View.INVISIBLE);
//				ll_btn_close.setVisibility(View.INVISIBLE);
//			} else {
//				ll_btn_back.setVisibility(View.VISIBLE);
//				ll_btn_close.setVisibility(View.VISIBLE);
//			}
//		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()){
			case R.id.ll_btn_back:
				finish();
				break;
			case R.id.ll_btn_edit:

				break;
		}
	}

	public void setIsBackButtonEnable(boolean isBackButtonEnable) {
		this.isBackButtonEnable = isBackButtonEnable;

		if (this.isBackButtonEnable){
			ll_btn_back.setVisibility(View.VISIBLE);
		}else{
			ll_btn_back.setVisibility(View.GONE);
		}
	}

	public void setIsEditButtonEnable(boolean isEditButtonEnable) {
		this.isEditButtonEnable = isEditButtonEnable;

		if (this.isEditButtonEnable){
			ll_btn_edit.setVisibility(View.VISIBLE);
		}else{
			ll_btn_edit.setVisibility(View.GONE);
		}
	}
}
