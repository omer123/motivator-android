package com.pm.motivator.networks.dialogs.npd;

import android.app.Activity;

/**
 * Created by faisalwahab on 10/12/15.
 */
public interface IProgressDialog {
    void init(Activity activity);
    void title(String title);
    void message(String message);
    void show();
    void hide();
}
