package com.pm.motivator.networks.retrofit;

import retrofit.RequestInterceptor;

/**
 * Created by user on 5/18/2015.
 */
public class SessionRequestInterceptor implements RequestInterceptor {
    @Override
    public void intercept(RequestInterceptor.RequestFacade request) {
//        if (user is connected)
        request.addHeader("Header name", "Header Value");
    }
}