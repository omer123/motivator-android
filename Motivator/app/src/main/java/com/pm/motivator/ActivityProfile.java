package com.pm.motivator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ActivityProfile extends ActivityBase {

    public ActivityProfile() {
        super(true, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
