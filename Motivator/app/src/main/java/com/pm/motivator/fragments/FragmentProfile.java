package com.pm.motivator.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pm.motivator.ActivityMain;
import com.pm.motivator.R;
import com.pm.motivator.adapters.AdapterProfileHabits;
import com.pm.motivator.interfaces.ICallBack;
import com.pm.motivator.models.ModelHabit;

import java.util.ArrayList;

/**
 * Created by faisalwahab on 10/19/15.
 */
public class FragmentProfile extends Fragment implements View.OnClickListener, ICallBack {
    private FragmentActivity mActivity = null;
    private ICallBack iCallBackA = null;
    private ICallBack iCallBackF = null;


    private ImageView iv_profile_img = null;
    private TextView tv_username = null;
    private TextView tv_email = null;
    private ListView lv_list = null;


    public static FragmentProfile newInstance(int pageLefts, String content, ICallBack iCallBackA) {
        FragmentProfile fragment = new FragmentProfile();
        Bundle args = new Bundle();
        args.putInt("PagesLeft", pageLefts);
        args.putString("Content", content);
        fragment.setArguments(args);

        return fragment;
    }

    public FragmentProfile() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            iCallBackF = (ICallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ICallBack");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iCallBackA = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        iv_profile_img = (ImageView) rootView.findViewById(R.id.iv_profile_img);
        tv_username = (TextView) rootView.findViewById(R.id.tv_username);
        tv_email = (TextView) rootView.findViewById(R.id.tv_email);
        lv_list = (ListView) rootView.findViewById(R.id.lv_list);

        ((ActivityMain) mActivity).setIsEditButtonEnable(true);
        tv_username.setText("Faisal Wahab");
        tv_email.setText("faisalwahab@live.com");

        AdapterProfileHabits adapterHabit = new AdapterProfileHabits(mActivity, getHabitList(), iCallBackA);
        lv_list.setAdapter(adapterHabit);

        return rootView;
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.iv_btn_home:
//                startActivity(new Intent(mActivity, ActivityMain.class));
//                break;
//            case R.id.iv_btn_fav:
//                if (ActivityMain.SELECTED_ITEM.selected) {
//
//                    for (int i = 0; i < AppData.LIST_CONTRIES.size(); i++) {
//                        if (AppData.LIST_CONTRIES.get(i).id == ActivityMain.SELECTED_ITEM.id) {
//                            AppData.LIST_CONTRIES.get(i).selected = false;
//                            ActivityMain.SELECTED_ITEM = AppData.LIST_CONTRIES.get(i);
//                            Prefrences.setAppData(AppData.LIST_CONTRIES);
//                            break;
//                        }
//                    }
//
//                    iv_btn_fav.setBackgroundResource(R.drawable.fav_non);
//
//                } else {
//
//                    for (int i = 0; i < AppData.LIST_CONTRIES.size(); i++) {
//                        if (AppData.LIST_CONTRIES.get(i).id == ActivityMain.SELECTED_ITEM.id) {
//                            AppData.LIST_CONTRIES.get(i).selected = true;
//                            ActivityMain.SELECTED_ITEM = AppData.LIST_CONTRIES.get(i);
//                            Prefrences.setAppData(AppData.LIST_CONTRIES);
//                            break;
//                        }
//                    }
//
//                    iv_btn_fav.setBackgroundResource(R.drawable.fav_agreed);
//
//                }
//
//                iCallBackA.onCallBack("", "");
//                break;
//            default:
//                break;
//        }
//
    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putInt(KEY_CONTENT, pageLefts);
//    }


    private ArrayList<ModelHabit> getHabitList() {

        ModelHabit habit_01 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_02 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_03 = new ModelHabit(null, false, "Habit with logo with  banner image");
        ModelHabit habit_04 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_05 = new ModelHabit(null, true, "Habit with logo with  banner image");
        ModelHabit habit_06 = new ModelHabit(null, false, "Habit with logo with no banner image");
        ModelHabit habit_07 = new ModelHabit(null, true, "Habit with logo with  banner image");
        ModelHabit habit_08 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_09 = new ModelHabit(null, false, "Habit with logo with no banner image");

        ArrayList<ModelHabit> listHabit = new ArrayList<ModelHabit>();
        listHabit.add(habit_01);
        listHabit.add(habit_02);
        listHabit.add(habit_03);
        listHabit.add(habit_04);
        listHabit.add(habit_05);
        listHabit.add(habit_06);
        listHabit.add(habit_07);
        listHabit.add(habit_08);
        listHabit.add(habit_09);
        return listHabit;
    }

    @Override
    public void notify(Object object, String action) {
        ModelHabit item = (ModelHabit) object;
        if (item != null) {
            if (iCallBackF != null) {
                iCallBackF.notify(item, "from_profile_habbit");
            }
        }
    }
}