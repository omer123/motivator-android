package com.pm.motivator;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TabHost;

import com.pm.motivator.fragments.FragmentFindHabbit;
import com.pm.motivator.fragments.FragmentHabbitDetails;
import com.pm.motivator.fragments.FragmentProfile;
import com.pm.motivator.interfaces.ICallBack;

public class ActivityMain extends ActivityBase implements ICallBack {
    private FragmentActivity mActivity = null;
    private TabHost mTabHost = null;

    private RadioButton menu_profile_rbtn = null;
    private RadioButton menu_find_rbtn = null;


    public ActivityMain() {
        super(true, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menu_profile_rbtn = (RadioButton) findViewById(R.id.menu_profile_rbtn);
        menu_find_rbtn = (RadioButton) findViewById(R.id.menu_find_rbtn);
    }


    public void onRadioButtonClick(View v) {
        switch (v.getId()) {
            case R.id.menu_profile_rbtn:

                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, new FragmentProfile()).commit();

                break;
            case R.id.menu_find_rbtn:

                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, new FragmentFindHabbit()).commit();

                break;
            default:
                break;
        }
    }

    @Override
    public void notify(Object object, String action) {
        if (action.equalsIgnoreCase("from_profile_habbit")||action.equalsIgnoreCase("from_find_habbit")) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, new FragmentHabbitDetails()).commit();
        }
    }
}
