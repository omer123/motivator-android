package com.pm.motivator;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivitySignUp extends AppCompatActivity implements View.OnClickListener {
    private FragmentActivity mActivity = null;

    private EditText et_username = null;
    private EditText et_email = null;
    private EditText et_password = null;
    private Button btn_signup = null;


    private String strUsername = null;
    private String strEmail = null;
    private String strPassword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mActivity = this;


        et_username = (EditText) findViewById(R.id.et_username);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_signup = (Button) findViewById(R.id.btn_signup);

        btn_signup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                strUsername = et_username.getText().toString();
                strEmail = et_email.getText().toString();
                strPassword = et_password.getText().toString();

                startActivity(new Intent(mActivity, ActivityMain.class));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, ActivitySignIn.class));
        super.onBackPressed();
    }
}
