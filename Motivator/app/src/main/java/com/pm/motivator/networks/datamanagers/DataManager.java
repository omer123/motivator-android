package com.pm.motivator.networks.datamanagers;

import android.support.v4.app.FragmentActivity;

import com.pm.motivator.networks.dialogs.npd.AppNetworkDialog;
import com.pm.motivator.networks.retrofit.RestCallback;
import com.pm.motivator.networks.retrofit.RestClient;
public class DataManager {
    private AppNetworkDialog apd = null;
    private RestClient client = new RestClient();

    public static DataManager getInstance() {
        return new DataManager();
    }

    private DataManager() {
    }

    public DataManager initNewProgress(FragmentActivity activity, AppNetworkDialog.ProgressType type) {
        apd = new AppNetworkDialog().initNewProgress(activity, type);
        return this;
    }


    public void getGoogleData(final RestCallback restCallback) {
        if (apd != null) {
            apd.showProgress();
        }
//        client.getApiService().getGoogleTestApi(new RestCallback<Example>() {
//            @Override
//            public void success(Example s, Response response) {
//                if (restCallback != null)
//                    restCallback.success(s, response);
//
//                if (apd != null) {
//                    apd.hideProgress();
//                }
//            }
//
//            @Override
//            public void failure(RestError restError) {
//                if (restCallback != null)
//                    restCallback.failure(restError);
//
//                if (apd != null) {
//                    apd.hideProgress();
//                }
//            }
//        });
    }
//    public void sendFeedBack( final ProgressDialog pd, final INetworkCallBack iNetworkCallBack) {
//        RequestMethodFactory.getInstance().post(SEND_FEEDBACK_ACTION, url, json, new INetworkWebSer() {
//            @Override
//            public void onCallStart() {
//                if (pd != null && !pd.isShowing()) {
//                    pd.show();
//                }
//            }
//
//            @Override
//            public void onCallEnd() {
//                if (pd != null && pd.isShowing()) {
//                    pd.dismiss();
//                }
//            }
//
//            @Override
//            public void onResponse(Object resultObj, String action) {
//                String response = (String) resultObj;
//                IcflixUtils.showLogs(response);
//                if (iNetworkCallBack != null) {
//                    iNetworkCallBack.onSuccess(response, action);
//                }
////						try {
////							ArrayList<DataModel> resList = new ArrayList<DataModel>();
////							Gson gson = new Gson();
////							resList = gson.fromJson(response,
////									new TypeToken<ArrayList<DataModel>>() {
////									}.getType());
////
////							if (iNetworkCallBack != null) {
////								if (resList == null || resList.isEmpty()) {
////									iNetworkCallBack.onFailed("No Data Found");
////								} else {
////									iNetworkCallBack.onSuccess(resList, action);
////								}
////							}
////						} catch (Exception e) {
////							e.printStackTrace();
////						}
//            }
//
//            @Override
//            public void onError(Object errorObj) {
//                if (iNetworkCallBack != null) {
//                    iNetworkCallBack.onFailed(errorObj);
//                }
//            }
//        });
//
//    }
}
