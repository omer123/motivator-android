package com.pm.motivator.networks.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by user on 5/18/2015.
 */
public class RestClient {
    private static final String BASE_URL = "http://www.google.com";
    private IAppApiServices apiService;

    public RestClient() {
        Gson gson = new GsonBuilder()
//                .registerTypeAdapterFactory(new ItemTypeAdapterFactory()) // This is the important line ;)
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
//                .setRequestInterceptor(new SessionRequestInterceptor()) // This is the important line ;)
                .build();

        apiService = restAdapter.create(IAppApiServices.class);
    }

    public IAppApiServices getApiService() {
        return apiService;
    }
}
