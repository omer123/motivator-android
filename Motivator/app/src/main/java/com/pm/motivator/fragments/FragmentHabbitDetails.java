package com.pm.motivator.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pm.motivator.ActivityMain;
import com.pm.motivator.R;
import com.pm.motivator.interfaces.ICallBack;

/**
 * Created by faisalwahab on 10/19/15.
 */
public class FragmentHabbitDetails extends Fragment implements ICallBack {
    private FragmentActivity mActivity = null;
    private ICallBack iCallBack = null;


    public static FragmentHabbitDetails newInstance(int pageLefts, String content, ICallBack iCallBackA) {
        FragmentHabbitDetails fragment = new FragmentHabbitDetails();
        Bundle args = new Bundle();
        args.putInt("PagesLeft", pageLefts);
        args.putString("Content", content);
        fragment.setArguments(args);

        return fragment;
    }

    public FragmentHabbitDetails() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iCallBack = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_habbit_details, container, false);

        ((ActivityMain) mActivity).setIsEditButtonEnable(false);


        return rootView;
    }


    @Override
    public void notify(Object object, String action) {

    }
}