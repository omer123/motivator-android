package com.pm.motivator.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.motivator.R;
import com.pm.motivator.interfaces.ICallBack;
import com.pm.motivator.models.ModelHabit;

import java.util.ArrayList;

public class AdapterProfileHabits extends BaseAdapter {
    private FragmentActivity mActivity = null;
    private ICallBack iCallBack = null;

    private ArrayList<ModelHabit> list = null;

    public AdapterProfileHabits(FragmentActivity mActivity, ArrayList<ModelHabit> list, ICallBack iCallBack) {
        this.mActivity = mActivity;
        this.list = list;
        this.iCallBack = iCallBack;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.layout_list_item_profile_habit, null);
            holder = new ViewHolder();

            holder.iv_icon = (ImageView) vi.findViewById(R.id.iv_icon);
            holder.tv_habit_title = (TextView) vi.findViewById(R.id.tv_habit_title);
            holder.iv_habit_bg = (ImageView) vi.findViewById(R.id.iv_habit_bg);

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        final ModelHabit habit = list.get(position);

        if (habit != null) {
            if (habit.isHabitIconShow()) {
                holder.iv_icon.setVisibility(View.VISIBLE);
            } else {
                holder.iv_icon.setVisibility(View.INVISIBLE);
            }

            if (habit.getTextHabit() != null) {
                holder.tv_habit_title.setText(habit.getTextHabit());
            }

            if (habit.getUrlHabitBg() != null && habit.getUrlHabitBg().length() > 0) {
                holder.iv_habit_bg.setVisibility(View.VISIBLE);
                // download and show image code
            } else {
                holder.iv_habit_bg.setVisibility(View.INVISIBLE);
            }
        }

        vi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iCallBack != null) {
                    iCallBack.notify(habit, "item_selected");
                }
            }
        });
        return vi;
    }

    public static class ViewHolder {
        public ImageView iv_icon;
        public TextView tv_habit_title;
        public ImageView iv_habit_bg;
    }

}
