package com.pm.motivator.networks.dialogs.npd;

import android.support.v4.app.FragmentActivity;

/**
 * Created by faisalwahab on 10/14/15.
 */
public class AppNetworkDialog {
    private IProgressDialog pd = null;

    public enum ProgressType {
        DIALOG, WHEEL
    }

    public AppNetworkDialog initNewProgress(FragmentActivity activity, ProgressType pType) {
        if (activity != null) {
            if (pType == ProgressType.WHEEL) {
                pd = HUDProgress.newInstance();
            } else if (pType == ProgressType.DIALOG) {
                pd = SimpleProgress.newInstance();
            }
            pd.init(activity);
        }
        return this;
    }

    public AppNetworkDialog setProgressMessage(String message) {
        if (pd != null) {
            pd.message(message);
        }
        return this;
    }

    public void showProgress() {
        if (pd != null) {
            pd.show();
        }
    }

    public void hideProgress() {
        if (pd != null) {
            pd.hide();
        }
    }
}
