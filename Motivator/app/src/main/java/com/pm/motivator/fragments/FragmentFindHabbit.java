package com.pm.motivator.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pm.motivator.ActivityMain;
import com.pm.motivator.R;
import com.pm.motivator.adapters.AdapterProfileHabits;
import com.pm.motivator.interfaces.ICallBack;
import com.pm.motivator.models.ModelHabit;

import java.util.ArrayList;

/**
 * Created by faisalwahab on 10/19/15.
 */
public class FragmentFindHabbit extends Fragment implements ICallBack {
    private FragmentActivity mActivity = null;
    private ICallBack iCallBackA = null;
    private ICallBack iCallBackF = null;


    private ListView lv_list = null;


    public static FragmentFindHabbit newInstance(int pageLefts, String content, ICallBack iCallBackA) {
        FragmentFindHabbit fragment = new FragmentFindHabbit();
        Bundle args = new Bundle();
        args.putInt("PagesLeft", pageLefts);
        args.putString("Content", content);
        fragment.setArguments(args);

        return fragment;
    }

    public FragmentFindHabbit() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            iCallBackF = (ICallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ICallBack");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iCallBackA = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_habbit, container, false);

        lv_list = (ListView) rootView.findViewById(R.id.lv_list);

        ((ActivityMain) mActivity).setIsEditButtonEnable(false);

        AdapterProfileHabits adapterHabit = new AdapterProfileHabits(mActivity, getHabitList(), iCallBackA);
        lv_list.setAdapter(adapterHabit);

        return rootView;
    }


    private ArrayList<ModelHabit> getHabitList() {

        ModelHabit habit_01 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_02 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_03 = new ModelHabit(null, false, "Habit with logo with  banner image");
        ModelHabit habit_04 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_05 = new ModelHabit(null, true, "Habit with logo with  banner image");
        ModelHabit habit_06 = new ModelHabit(null, false, "Habit with logo with no banner image");
        ModelHabit habit_07 = new ModelHabit(null, true, "Habit with logo with  banner image");
        ModelHabit habit_08 = new ModelHabit(null, true, "Habit with logo with no banner image");
        ModelHabit habit_09 = new ModelHabit(null, false, "Habit with logo with no banner image");

        ArrayList<ModelHabit> listHabit = new ArrayList<ModelHabit>();
        listHabit.add(habit_01);
        listHabit.add(habit_02);
        listHabit.add(habit_03);
        listHabit.add(habit_04);
        listHabit.add(habit_05);
        listHabit.add(habit_06);
        listHabit.add(habit_07);
        listHabit.add(habit_08);
        listHabit.add(habit_09);
        return listHabit;
    }

    @Override
    public void notify(Object object, String action) {
        ModelHabit item = (ModelHabit) object;
        if (item != null) {
            if (iCallBackF != null) {
                iCallBackF.notify(item, "from_find_habbit");
            }
        }
    }
}